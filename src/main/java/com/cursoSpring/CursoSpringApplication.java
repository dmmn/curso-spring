package com.cursoSpring;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.cursoSpring.dao.AddressDAO;
import com.cursoSpring.dao.CategoryDAO;
import com.cursoSpring.dao.CityDAO;
import com.cursoSpring.dao.ClientDAO;
import com.cursoSpring.dao.EstateDAO;
import com.cursoSpring.dao.OrderDAO;
import com.cursoSpring.dao.OrderItemDAO;
import com.cursoSpring.dao.PaymentDAO;
import com.cursoSpring.dao.ProductDAO;
import com.cursoSpring.model.AddressModel;
import com.cursoSpring.model.CategoryModel;
import com.cursoSpring.model.CityModel;
import com.cursoSpring.model.ClientModel;
import com.cursoSpring.model.EstateModel;
import com.cursoSpring.model.OrderItemModel;
import com.cursoSpring.model.OrderModel;
import com.cursoSpring.model.PaymentModel;
import com.cursoSpring.model.PaymentWithBilletModel;
import com.cursoSpring.model.PaymentWithCreditCardModel;
import com.cursoSpring.model.ProductModel;
import com.cursoSpring.model.enums.ClientType;
import com.cursoSpring.model.enums.PaymentType;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"com.cursoSpring.model"})
public class CursoSpringApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CursoSpringApplication.class, args);
	}
	
	@Autowired
	private CategoryDAO categoryDAO;
	@Autowired
	private ProductDAO productDAO;
	@Autowired 
	private CityDAO cityDAO;
	@Autowired
	private EstateDAO estateDAO;
	@Autowired
	private ClientDAO clientDAO;
	@Autowired 
	private AddressDAO addressDAO;
	@Autowired
	private OrderDAO orderDAO;
	@Autowired 
	private PaymentDAO paymentDAO;
	@Autowired
	private OrderItemDAO orderItemDAO;

	@Override
	public void run(String... args) throws Exception {
		CategoryModel cat1 = new CategoryModel(null, "Informática");
		CategoryModel cat2 = new CategoryModel(null, "Escritório");
		ProductModel prod1 = new ProductModel(null, "Computador", 2000.00);
		ProductModel prod2 = new ProductModel(null, "Impressora", 800.00);
		ProductModel prod3 = new ProductModel(null, "Mouse", 80.00);
		cat1.getProducts().addAll(Arrays.asList(prod1, prod3));
		cat2.getProducts().addAll(Arrays.asList(prod2));
		prod1.getCategories().addAll(Arrays.asList(cat1));
		prod2.getCategories().addAll(Arrays.asList(cat1, cat2));
		prod3.getCategories().addAll(Arrays.asList(cat1));
		categoryDAO.saveAll(Arrays.asList(cat1, cat2));
		productDAO.saveAll(Arrays.asList(prod1, prod2, prod3));
		
		EstateModel estate1 = new EstateModel(null, "Minas Gerais", "MG");
		EstateModel estate2 = new EstateModel(null, "São Paulo", "SP");
		CityModel city1 = new CityModel(null, "Uberlândia", estate1);
		CityModel city2 = new CityModel(null, "São Paulo", estate2);
		CityModel city3 = new CityModel(null, "Campinas", estate2);
		estateDAO.saveAll(Arrays.asList(estate1, estate2));
		cityDAO.saveAll(Arrays.asList(city1, city2, city3));
		
		ClientModel client1 = new ClientModel(null, "Maria Silva", "mariasilva@gmail.com", "390.998.965-33", ClientType.PRIVATE_INDIVIDUAL);
		AddressModel addr1 = new AddressModel(null, "Rua Flores", "300", "apto 303", "Jardim", "01226-011", client1, city1);
		AddressModel addr2 = new AddressModel(null, "Avenida Matos", "105", "sala 800", "Centro", "01455-000", client1, city2);
		client1.getAdresses().addAll(Arrays.asList(addr1, addr2));
		client1.getPhones().addAll(Arrays.asList("11 99854-2554", "11 96658-5554"));
		clientDAO.saveAll(Arrays.asList(client1));
		addressDAO.saveAll(Arrays.asList(addr1, addr2));
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		
		OrderModel order1 = new OrderModel(null, sdf.parse("10/07/2020 14:20"), client1, addr1);
		OrderModel order2 = new OrderModel(null, sdf.parse("15/12/2020 13:55"), client1, addr2);
		PaymentModel pay1 = new PaymentWithCreditCardModel(null, PaymentType.PAID_OUT, order1, 6);
		order1.setPayment(pay1);
		PaymentModel pay2 = new PaymentWithBilletModel(null, PaymentType.PENDING, order2, sdf.parse("25/12/2020 13:55"), null);
		order2.setPayment(pay2);
		client1.getOrders().addAll(Arrays.asList(order1, order2));
		orderDAO.saveAll(Arrays.asList(order1, order2));
		paymentDAO.saveAll(Arrays.asList(pay1, pay2));
		
		OrderItemModel o1 = new OrderItemModel(order1, prod1, 0.0, 2000.00, 1);
		OrderItemModel o2 = new OrderItemModel(order1, prod3, 0.0, 80.00, 2);
		OrderItemModel o3 = new OrderItemModel(order2, prod2, 100.00, 800.00,1);
		order1.getOrderItens().addAll(Arrays.asList(o1, o2));
		order2.getOrderItens().addAll(Arrays.asList(o3));
		prod1.getOrderItens().addAll(Arrays.asList(o1));
		prod2.getOrderItens().addAll(Arrays.asList(o3));
		prod3.getOrderItens().addAll(Arrays.asList(o2));
		orderItemDAO.saveAll(Arrays.asList(o1, o2, o3));	
	}
}
