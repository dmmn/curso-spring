package com.cursoSpring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cursoSpring.dao.OrderDAO;
import com.cursoSpring.model.OrderModel;
import com.cursoSpring.services.exceptions.ObjectNotFoundException;

@Service
public class OrderService {

	@Autowired
	private OrderDAO orderDAO;
	
	public OrderModel findById(Integer id) {
		return orderDAO.findById(id).orElseThrow(
				() -> new ObjectNotFoundException("Object not found! ID: " + id + " Type: " + OrderModel.class.getName())
		);
	}
	
	public List<OrderModel> findAll(){
		return orderDAO.findAll();
	}
	
}
