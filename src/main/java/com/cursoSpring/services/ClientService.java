package com.cursoSpring.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cursoSpring.dao.ClientDAO;
import com.cursoSpring.model.ClientModel;
import com.cursoSpring.services.exceptions.ObjectNotFoundException;

@Service
public class ClientService {
	
	@Autowired
	private ClientDAO clientDAO;
	
	public ClientModel findById(Integer id) {
		return clientDAO.findById(id).orElseThrow(
				() -> new ObjectNotFoundException("Object not found! ID: " + id + " Type: " + ClientModel.class.getName())
		);
	}
	
	public List<ClientModel> findAll(){
		return clientDAO.findAll();
	}
}
