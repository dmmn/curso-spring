package com.cursoSpring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cursoSpring.dao.CategoryDAO;
import com.cursoSpring.model.CategoryModel;
import com.cursoSpring.services.exceptions.ObjectNotFoundException;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryDAO CategoryDAO;
	
	public CategoryModel findById(Integer id) {
		return CategoryDAO.findById(id).orElseThrow(
				() -> new ObjectNotFoundException("Object not found! ID: " + id + " Type: " + CategoryModel.class.getName())
		);
	}
}
