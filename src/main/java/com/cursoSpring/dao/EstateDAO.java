package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoSpring.model.EstateModel;

public interface EstateDAO extends JpaRepository<EstateModel, Integer>{

}
