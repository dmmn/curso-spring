package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoSpring.model.ProductModel;

public interface ProductDAO extends JpaRepository<ProductModel, Integer>{

}
