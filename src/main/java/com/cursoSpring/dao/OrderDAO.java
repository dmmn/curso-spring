package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cursoSpring.model.OrderModel;

@Repository
public interface OrderDAO extends JpaRepository<OrderModel, Integer>{

}
