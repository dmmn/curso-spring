package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoSpring.model.AddressModel;

public interface AddressDAO extends JpaRepository<AddressModel, Integer>{

}
