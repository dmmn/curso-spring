package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoSpring.model.PaymentModel;

public interface PaymentDAO extends JpaRepository<PaymentModel, Integer>{

}
