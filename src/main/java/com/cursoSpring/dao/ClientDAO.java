package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoSpring.model.ClientModel;

public interface ClientDAO extends JpaRepository<ClientModel, Integer>{

}
