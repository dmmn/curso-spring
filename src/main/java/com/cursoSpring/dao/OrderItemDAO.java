package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoSpring.model.OrderItemModel;

public interface OrderItemDAO extends JpaRepository<OrderItemModel, Integer>{

}
