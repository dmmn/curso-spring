package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cursoSpring.model.CityModel;

public interface CityDAO extends JpaRepository<CityModel, Integer> {

}
