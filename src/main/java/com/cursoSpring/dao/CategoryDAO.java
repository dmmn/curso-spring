package com.cursoSpring.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cursoSpring.model.CategoryModel;

@Repository
public interface CategoryDAO extends JpaRepository<CategoryModel, Integer>{
	
}
