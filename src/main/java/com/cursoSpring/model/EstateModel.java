package com.cursoSpring.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "estate")
public class EstateModel implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "name")
	private String name;
	@Column(name = "initials")
	private String initials;
	@OneToMany(mappedBy = "estate")
	private List<CityModel> cities = new ArrayList<CityModel>();
	
	public EstateModel() {}
	
	public EstateModel(Integer id, String name, String initials) {
		this.id = id;
		this.name = name;
		this.initials = initials;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInitials() {
		return initials;
	}

	public void setInitials(String initials) {
		this.initials = initials;
	}

	public List<CityModel> getCities() {
		return cities;
	}

	public void setCities(List<CityModel> cities) {
		this.cities = cities;
	}
}
