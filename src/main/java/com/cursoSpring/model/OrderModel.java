package com.cursoSpring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "orders")
public class OrderModel implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "date")
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm")
	private Date date;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "order")
	private PaymentModel payment;
	@ManyToOne
	@JoinColumn(name = "client_id")
	private ClientModel client;
	@ManyToOne
	@JoinColumn(name = "address_id")
	private AddressModel address;
	@OneToMany(mappedBy = "id.order")
	private Set<OrderItemModel> orderItens = new HashSet<OrderItemModel>();
	
	public OrderModel() {}
	
	public OrderModel (Integer id, Date date, ClientModel client, AddressModel address) {
		this.id = id;
		this.date = date;
		this.client = client;
		this.address = address;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public PaymentModel getPayment() {
		return payment;
	}

	public void setPayment(PaymentModel payment) {
		this.payment = payment;
	}

	public ClientModel getClient() {
		return client;
	}

	public void setClient(ClientModel client) {
		this.client = client;
	}

	public AddressModel getAddress() {
		return address;
	}

	public void setAddress(AddressModel address) {
		this.address = address;
	}
	
	public Set<OrderItemModel> getOrderItens() {
		return orderItens;
	}

	public void setOrderItens(Set<OrderItemModel> orderItens) {
		this.orderItens = orderItens;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderModel other = (OrderModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
