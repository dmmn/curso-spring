package com.cursoSpring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.cursoSpring.model.enums.PaymentType;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "payment_billet")
public class PaymentWithBilletModel extends PaymentModel{
	private static final long serialVersionUID = 1L;
	@Column(name = "expiration_date")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date expirationDate;
	@Column(name = "payment_date")
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Date paymentDate;
	
	public PaymentWithBilletModel() {}

	public PaymentWithBilletModel(Integer id, PaymentType paymentType, OrderModel order, Date expirationDate, Date paymentDate) {
		super(id, paymentType, order);
		this.expirationDate = expirationDate;
		this.paymentDate = paymentDate;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
}
