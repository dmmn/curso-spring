package com.cursoSpring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.cursoSpring.model.enums.PaymentType;

@Entity
@Table(name = "payment_card")
public class PaymentWithCreditCardModel extends PaymentModel{
	private static final long serialVersionUID = 1L;
	
	@Column(name = "installments")
	private Integer installments;
	
	public PaymentWithCreditCardModel() {}

	public PaymentWithCreditCardModel(Integer id, PaymentType paymentType, OrderModel order, Integer installments) {
		super(id, paymentType, order);
		this.installments = installments;
	}

	public Integer getInstallments() {
		return installments;
	}

	public void setInstallments(Integer installments) {
		this.installments = installments;
	}
}
