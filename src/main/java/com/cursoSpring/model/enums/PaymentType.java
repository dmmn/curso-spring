package com.cursoSpring.model.enums;

public enum PaymentType {
	
	PENDING (1, "Pending"),
	PAID_OUT (2, "Paid Out"),
	CANCELED (3, "Canceled");
	
	private Integer cod;
	private String description;
		
	private PaymentType(Integer cod, String description) {
		this.cod = cod;
		this.description = description;
	}
	
	public Integer getCod() {
		return cod;
	}
	public String getDescription() {
		return description;
	}
	
	public static PaymentType toEnum(Integer cod) {
		if(cod == null) {
			return null;
		}
		for(PaymentType payment : PaymentType.values()) {
			if(cod.equals(payment.getCod())) {
				return payment;
			}
		}
		throw new IllegalArgumentException("Invalid ID: " + cod);
	}
}
