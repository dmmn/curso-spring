package com.cursoSpring.model.enums;

public enum ClientType {
	PRIVATE_INDIVIDUAL(1, "Pessoa Física"),
	LEGAL_ENTITY(2, "Pessoa Jurídica");
	
	private Integer cod;
	private String description;
	
	private ClientType(Integer cod, String description) {
		this.cod = cod;
		this.description = description;
	}
	
	public Integer getCod() {
		return cod;
	}
	public String getDescription() {
		return description;
	}
	
	public static ClientType toEnum(Integer cod) {
		if(cod == null) {
			return null;
		}
		for(ClientType client : ClientType.values()) {
			if(cod.equals(client.getCod())) {
				return client;
			}
		}
		throw new IllegalArgumentException("Invalid ID: " + cod);
	}
}
